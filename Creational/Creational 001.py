"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Example of Singleton Design Pattern
"""


class Singleton(object):

    instance = None

    def __new__(cls):
        """
            Logic is to store the single instance as class variable.
            if the class variable does not exist, then create a new one and return it.
            if the class variable exists, then return it.
        """
        
        if Singleton.instance == None:
            Singleton.instance = super(Singleton, cls).__new__(cls)
        
        return Singleton.instance


m = Singleton()
s = Singleton()
z = Singleton()

print(m)
print(s)
print(z)
print(m.instance)
print(s.instance)
print(z.instance)

