'''
Author : RyuCoder
Website: http://ryucoder.in/
Example: Simple Function Decorator Example
'''


def decorator(func): # func must be a callable, in this case a function
    def wrapper():
        # Code that needs to be run before the func was called
        print("Before func is called.")
        
        func() # function main is being called here

        # Code that needs to be run after the func was called
        print("After func is called.")

        return None # Wrapper function can return some value, it is optional
    return wrapper # decorator function must return a callable, in this case a function

@decorator
def main():
    print("Main was called.")
# main = decorator(main)

if __name__ == "__main__":
    main()