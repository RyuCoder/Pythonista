"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Example of multiple decorators - function based, class based and instance based
"""


def FunctionDecorator(func):
    def inner():
        print("FunctionDecorator Before")
        func()
        print("FunctionDecorator After")

    return inner


class ClassDecorator():

    def __init__(self, func):
        self.func = func

    def __call__(self):
        print("ClassDecorator Before")
        self.func()
        print("ClassDecorator After")


class InstanceDecorator():

    def __call__(self, func):

        def inner():
            print("InstanceDecorator Before")
            func()
            print("InstanceDecorator After")
        
        return inner


Instance = InstanceDecorator()


@FunctionDecorator
@ClassDecorator
@Instance
def main():
    print("MAIN")


if __name__ == "__main__":
    main()
    






