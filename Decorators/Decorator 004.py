
def decorate_inner(func, arg1, arg2):
    print("decorate_inner Called.")
    print(func, " from decorate_inner")
    print(arg1, " from decorate_inner")
    print(arg2, " from decorate_inner")

    def inner(text):
        print("Inner Called.")
        print(arg1, " from inner")
        print(arg2, " from inner")
        print(text + " from inner.")

    return inner


# @decorate_inner("arg1", "arg2")
def second(text):
    print("Second Called.")

second = decorate_inner(second, "arg1", "arg2")

second("Love")
