'''
Author : RyuCoder
Website: http://ryucoder.in/
Example: Simple Class Decorator Example
'''

class Decorator():

    def __init__(self, f):
        self.f = f
        self.count = 0

    def __call__(self):
        self.count += 1
        return self.f()


@Decorator
def main():
    print("Main was called.")
# main = Decorator(main)

if __name__ == "__main__":
    print(main)
    main()
    main()
    print(main.count)
    main()
    main()
    print(main.count)