"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Example of multiple decorators - before decorator
"""

def decorator1(fn):
    print("decorator1")
    # print(fn)
    def inner1(*args, **kwargs):
        print("inner1")
        return fn(*args, **kwargs)
    return inner1

def decorator2(fn):
    print("decorator2")
    # print(fn)
    def inner2(*args, **kwargs):
        print("inner2")
        return fn(*args, **kwargs)
    return inner2

@decorator1
@decorator2
def foo():
    print("foo")


# foo  = decorator1(decorator2(foo))

foo()
