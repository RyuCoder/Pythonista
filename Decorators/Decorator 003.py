'''
Author : RyuCoder
Website: http://ryucoder.in/
Example: Simple Class Instance Decorator Example
'''

class Decorator():

    def __init__(self):
        self.count = 0

    def __call__(self, func):

        def wrapper():
            self.count += 1
            return func()

        return wrapper


Decorator_Instance = Decorator()

@Decorator_Instance
def main():
    print("Main was called.")
# main = Decorator_Instance(main)

if __name__ == "__main__":
    print(main)
    main()
    main()
    print(Decorator_Instance.count)
    main()
    main()
    print(Decorator_Instance.count)