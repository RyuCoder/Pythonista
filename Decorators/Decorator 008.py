"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Example of Class Decorator with arguments
"""


class ClassDecorator():

    def __init__(self, *args, **kwargs):
        self.args = args 
        self.kwargs = kwargs 

    def __call__(self, func):
        def inner():
            print("ClassDecorator Before")
            print(self.args)
            print(self.kwargs)
            func()
            print("ClassDecorator After")

        return inner


@ClassDecorator(3,4,5, name="Dragon") # __init__ gets called here
def main():
    print("MAIN")


if __name__ == "__main__":
    main() # inner gets called here
    






