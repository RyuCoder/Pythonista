def decorate_outer(arg1, arg2):
    print("Decorate_outer Called.")
    print(arg1)
    print(arg2)

    def decorate_inner(func):
        print("decorate_inner Called.")
        print(func, " from decorate_inner")

        def inner(text):
            print("Inner Called.")
            print(text + " from inner.")

        return inner

    return decorate_inner


@decorate_outer("arg1", "arg2")
def second(text):
    print("Second Called.")


second("Love")
