"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Document the list of future examples.
"""

Types of decorator -
- Function decorators
- Class decorators
- Class Instance decorators


"""
Below types are not the standard types. 
They are my undestanding of decorators and only for the ease of the understanding.
"""
Each Type can again be devided into the following subtypes 
For the ease of use assume that a function named foo is being decorated
- Before decorator
    All the decorator functions are being executed before the function foo. It is sort of setUp function in unittesting. 
- BeAf decorator
    All the decorator functions are being executed before and after the function foo. 
- After decorator
    All the decorator functions are being executed after the function foo. It is sort of wrapUp function in unittesting. 



Examples - 

* Function decorators for functions -
Purpose: Example of single decorator - before decorator
    Create a folder, create a file (useful in unittesting for making sure that file and folders exists)
    (check_or_create decorator for files and folders)
    foo()
    
Purpose: Example of single decorator - BeAf decorator
    Create a folder, create a file
    foo()
    delete file, delete a folder

Purpose: Example of single decorator - after decorator
    foo()
    delete file, delete a folder

Purpose: Example of single decorator - passing parameters simple way
Purpose: Example of single decorator - passing parameters decorator way
Purpose: Example of single decorator - decorating the inner function of the decorator

Purpose: Example of multiple decorators - before decorator
Purpose: Example of multiple decorators - BeAf decorator
Purpose: Example of multiple decorators - after decorator
Purpose: Example of multiple decorators - before and after decorator
Purpose: Example of multiple decorators - passing parameters simple way
Purpose: Example of multiple decorators - passing parameters decorator way
Purpose: Example of multiple decorators - decorating the inner function of the decorator


* Class decorators for functions -


* Decorators for class methods. 
These decorators can be functions, classes or class instances

Single decorator
Multiple decorator


Uses of decorators - 
1.Check if certain conditions are met before invoking function foo - Before Decorator
e.g. 
1. user is logged in. 
2. user has rights to do that action
3. function arguments satishfy the criteria

2. Run some commands after the foo function is called - After Decorator
e.g.
1. Close an open file
2. Delete files and folders after the tests are run
3. Send a signal so that some other code can be notified of an event


3. Check something before foo is run and run some commands after foo function is called - BeAf Decorator.


*************************************

import os
from subprocess import call
from pprint import pprint

print = pprint

def main():
    text = os.getenv("PATH").split(":")
    # text = os.system("ls")
    # print(text)
    print()
    c = call(["pwd"])
    # c = call(["ls", "-la"])
    print(c)
    print(type(c))


if __name__ == '__main__':
    main()

*************************************


example of - 
GeneratorExit
gen.throw()
ge.close()


Uses of Coroutine - ???
Multi threading and multi procesing



*************************************

Change variable names in the Scopes folder files 

*************************************

















