"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Examples to send keyword arguments into a coroutine
Known issues: None
"""


def generator(lucky_number, name="RyuCoder", **kwargs):
    print()
    print("From Generator:", lucky_number)
    print("From Generator:", name )
    print("From Generator:", kwargs )
    print()

    while True:
        val = yield 
        print("From Generator:", val)


def main():

    gen = generator(3, proname="RyuCoder")
    # gen = generator(3, name="Dragon", proname="RyuCoder")

    print("Starting the coroutine: ", next(gen)) 
    print()

    try:
        # Can send a integer
        print(gen.send(5))

        # Can't send keyword arguments
        # This won't work at all
        # print(gen.send(age=5))

    except StopIteration: 
        pass


if __name__ == "__main__":
    main()
