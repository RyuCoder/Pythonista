"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Examples of what can be send into a coroutine
Known issues: None
"""


def generator():
    
    while True:
        val = yield 
        print("From Generator:", val)


def main():

    gen = generator()

    # coroutine needs to be started before we can send values
    print("Starting the coroutine: ", next(gen))
    print()
    
    try:
        # Can send a integer
        print(gen.send(5))

        # Can send a float
        print(gen.send(5.5))

        # Can send a string
        print(gen.send("5")) 
        
        # Can send a list
        print(gen.send([5, 55, 555])) 
        
        # Can send a dictionary
        print(gen.send({"age":3, "name":"Dragon"}))

        # Can send a tuple
        print(gen.send((5, 55, 555))) 


        # Can't send more than one value 
        # print(gen.send("5", "SOS")) 
        
        # Can't send keyword arguments
        # print(gen.send(age=5)) 

    except StopIteration: 
        pass


if __name__ == "__main__":
    main()
