"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Types of a coroutine - OFU (Only For Understanding)
Known issues: None
"""


def infinite_coroutine():

    message = "Damn! I won't be printed at all."

    while True:
        val = yield 
        print("From Infinite Generator:", val)

    print(message)


def finite_coroutine():

    message = "Phew! I am gonna be printed."
    
    while True:
        val = yield
        print("From Finite Generator:", val)

        if val == 0: # some condition that will break the loop
            break

    print()
    print(message)


def run_infinite_coroutine():
    # StopIteration Exception does not need to be handled.
    
    infinite = infinite_coroutine()
    
    print("Starting the infinite coroutine: ", next(infinite))
    print()

    print(infinite.send(5))
    print(infinite.send(6))
    print(infinite.send(2))
    print(infinite.send(8))
    print(infinite.send(4))
    
    print()


def run_finite_coroutine():
    # StopIteration Exception needs to be handled.

    finite = finite_coroutine()

    print("Starting the finite coroutine: ", next(finite))
    print()

    try:
        print(finite.send(5))
        print(finite.send(15))
        print(finite.send(0))
        print(finite.send(25)) # this code wont execute

    except StopIteration:
        pass


def main():

    run_infinite_coroutine()
    
    print("********************************")

    run_finite_coroutine()


if __name__ == "__main__":
    main()
