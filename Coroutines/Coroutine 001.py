"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Simplest working example of a coroutine
Known issues: None
Notes: 
Functions: Are subset of Generators
    Can take values once
    Can return values once

Generators: Are subset of Coroutines
    Can take values once
    Can return values n no of times

Coroutines : Are superset of Functions and Generators
    Can take values n no of times 
    Can return values n no of times
"""


def generator():
    val = yield 
    print("Hi " + str(val))


def main():

    gen = generator()

    try:
        print(next(gen)) # coroutine needs to be started before we can send values
        print(gen.send(5)) # 5 will be stored in the variable val
    except StopIteration: 
        # After printing "Hi 5", as there is no for/while loop
        # StopIteration exception will be raised
        pass


if __name__ == "__main__":
    main()
