"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: A working example of asyncio
Known issues: None
"""

import asyncio
import random


async def highest(x, y):
    print("Highest")

    for i in range(x, y):
        print(i)
        # print()
        await asyncio.sleep(random.randint(1, 9) / 10)
        print(i)
        print()


async def route():
    await asyncio.wait([
        highest(6, 11),
        highest(1, 6),
    ])
    print("route")


def main():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(route())
    loop.close()


if __name__ == "__main__":
    main()
