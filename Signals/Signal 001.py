"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Example of simple signal in Python 
Ref: https://www.journaldev.com/16039/python-signal-processing
Known Issues: 
    1. Code does not run in Run window of Pycharm and in Output window of VS Code
    Solution - Run the file in the terminal in Pycharm as well as VS Code using below command.
        python Signals\ 001.py 
    # \ is added to escape the space before 001.
"""

import signal
import time

# print(dir(signal))


def handled(signum, stackframe):
    print()
    print("Handler was called.")
    print("Signal Number:" + str(signum) + " Frame: " + str(stackframe))
    print()


def main():
    # assign the handler function named handled to the signal SIGINT
    sig = signal.signal(signal.SIGINT, handled)

    while 1:
        print()
        print("Press ctrl + c to fire the signal.SIGINT event.")  
        time.sleep(3)
        signal.pause()  # wait for signal.SIGINT event to be fired.


if __name__ == "__main__":
    main()
