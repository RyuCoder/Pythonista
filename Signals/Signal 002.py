"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Example of simple signal in Python 
Ref: https://www.journaldev.com/16039/python-signal-processing
Known Issues: None
"""

import signal
import time


def alarm_handler(signum, stackframe):
    print('Alarm at: ' + str(time.ctime()))


def main():
    # assign alarm_handler to SIGALARM
    signal.signal(signal.SIGALRM, alarm_handler)

    signal.alarm(1)  # set alarm after 4 seconds

    print('Current time: ' + str(time.ctime()))
    time.sleep(2)  # make sufficient delay for the alarm to happen


if __name__ == "__main__":
    main()
