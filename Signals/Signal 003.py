"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Get list of signals registered in the current OS 
Ref: https://pymotw.com/2/signal/
Known Issues: None
Notes: 
1. SIG_DFL is default handler for the signal
2. SIG_IGN means that this signal will be ignored
"""


import signal


def SIGALRM_handler(n, stack):
    return

# signal.signal(signal.SIGALRM, SIGALRM_handler)

def main():
    signals_to_names = {}

    for n in dir(signal):
        if n.startswith('SIG') and not n.startswith('SIG_'):
            signals_to_names[getattr(signal, n)] = n

    for s, name in sorted(signals_to_names.items()):
        handler = signal.getsignal(s)
        if handler is signal.SIG_DFL:
            handler = 'SIG_DFL'
        elif handler is signal.SIG_IGN:
            handler = 'SIG_IGN'
        print('%-10s (%2d):' % (name, s), handler)


if __name__ == "__main__":
    main()
