

firstname = "Dragon"
proname = "Draconis"


def main():

    proname = "RyuCoder"

    print("From main()")
    print(firstname)  # Reading from global scope
    print(proname)  # Reading from local scope

    def outer():
        firstname = "RYU"
        lastname = "Coder"

        print()
        print("From outer()")
        print(firstname)  # Reading from local scope
        print(proname)  # Reading from non-local scope not from global scope

        def inner():
            nonlocal lastname
            global proname

            print()
            print("From inner()")
            print(firstname)  # Reading from non-local scope
            print(proname)  # Reading from global scope
            print(lastname)  # Reading from non-local scope

        inner()

    outer()


if __name__ == '__main__':
    main()
