

firstname = "Dragon"
proname = "Proname"


def main():

    proname = "RyuCoder"
    lastname = "lastname"

    print("From main()")
    print(firstname)  # Reading from global scope
    print(proname)  # Reading from local scope

    def outer():
        proname = "Coder"
        nonlocal lastname # defining nonlocal explicitely

        print()
        print("From outer()")
        print(proname)  # Reading from local scope
        print(lastname)  # Reading from non-local scope

    outer()


if __name__ == '__main__':
    main()
