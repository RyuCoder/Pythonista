

x = 3
y = 2


def add():
    return x + y  # Both x and y from global scope


def sub():
    x = 4
    y = 2
    return x - y  # Both x and y from local scope


def mul():
    y = 6
    return x * y  # x from global scope and y from local scope


def main():
    a = add()
    s = sub()
    m = mul()

    print(a)
    print(s)
    print(m)


if __name__ == '__main__':
    main()
