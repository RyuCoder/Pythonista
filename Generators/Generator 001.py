'''
Author : RyuCoder
Website: http://ryucoder.in/
Example: Simple Generator Function
Known Issues: None
'''


def generator():
    print("Enter generator")
    # yield # yield is similar to return or return None 
    yield 0
    print("Exit generator")


def main():
    gen1 = generator()
    gen21 = generator()
    gen22 = generator()
    gen3 = generator()

    # there are 3 ways to iterate over a generator 
    # 1. Using for loop
    for i in gen1:
        print(i)  # value of i will the the return value of the generator i.e. value that will be yielded
    # StopIteration error will not be raised as it is handled by Python Interpreter.

    print("****************************************")

    # 2. Manually using built-in method of python i.e. next()
        # 2.1 
    next(gen21)  # line no 13 i.e. print("Exit generator") will not be executed. 
    # StopIteration error will not be raised.

        # 2.2
    try:
        next(gen22)  # line no 13 i.e. print("Exit generator") will not be executed. 
        
        next(gen22)  # line no 13 will be executed. After that StopIteration error will be raised.
        # thats why we need to use try catch block to iterate over the generator manually.

    except StopIteration: 
        pass

    print("****************************************")

    # 3. while loop is also possible.
    is_exhausted = False

    while(not is_exhausted):
        try:
            next(gen3)
        except StopIteration:
            is_exhausted = True


if __name__ == "__main__":
    main()
    