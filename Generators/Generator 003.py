def main():
    print()
    print("Before For Loop")

    for i in range(3):
        print()
        print("Before yield")
        yield
        print("After yield")
        print()

    print("After For Loop")
    print()

if __name__ == "__main__":
    main = main()
    
    # try:
    #     print(next(main))    
    #     print(next(main))    
    #     print(next(main))    
    #     print(next(main))    
    # except StopIteration:
    #     pass 

    for i in main:
        print(i)
