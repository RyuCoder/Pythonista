
def main():
    print()
    print("Before yield")
    yield # yield is logically equal to return or return None
    print("After yield")
    print()


if __name__ == "__main__":
    main = main()
    try:
        print(next(main))    
        print(next(main))    
    except StopIteration:
        pass 


 