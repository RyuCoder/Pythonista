"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Example of Instance Decorator with arguments
"""


class InstanceDecorator():

    def __call__(self, *args, **kwargs):
        def outer(func):
            def inner():
                print("InstanceDecorator Before")
                print(args)
                print(kwargs)
                func()
                print("InstanceDecorator After")
            
            return inner
        return outer


instance = InstanceDecorator()

@instance(1,2,3, name="Dragon")
def main():
    print("MAIN")


if __name__ == "__main__":
    main()
    