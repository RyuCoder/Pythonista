"""
Author: RyuCoder
Website: http://ryucoder.in/
Purpose: Simplest example of scalable code
Known issues: None
"""


def get_item_problem(item):
    return item


def get_item_solution(*item): # This code is scalable code
    return item


def main():

    x = get_item_problem(3) # Code works fine
    # x = get_item_problem(3, 33, 333) # Code breaks
    print(x)

    # Below code works fine without breaking
    # It is scalable code
    # x = get_item_solution(3)
    x = get_item_solution(3, 33)
    # x = get_item_solution(3, 33, 333)
    print(x)


if __name__ == '__main__':
    main()
