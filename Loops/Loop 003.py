# purpose : while loop with 1 variable and 1 boolean value

def main():
    
    count = 0

    while True:

        count += 1

        if count == 3:
            continue # 3 won't be printed
        
        print(count)

        if count == 5:
            break # stops while loop
        

if __name__ == "__main__":
    main()
