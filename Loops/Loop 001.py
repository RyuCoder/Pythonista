


def main():

    for i in range(5):
        print(i)
    
    print("*********************")

    items = [1, 2, 3, 4, 5]

    for i in items:
        print(i)

    print("*********************")

    for index, i in enumerate(items):
        print(i, index)
    

if __name__ == "__main__":
    main()
