# purpose : while loop with 2 variables

def main():

    flag = True
    count = 0 

    while flag != False:
        print("hi")
        
        count += 1

        if count == 5:
            flag = False


if __name__ == "__main__":
    main()
