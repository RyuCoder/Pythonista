# purpose : while loop with 1 variables

def main():
    
    count = 0 

    while count < 5:
        print(count)
        count += 1


if __name__ == "__main__":
    main()
