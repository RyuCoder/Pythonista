
def main():

    count = 0

    while True:

        count += 1

        print(count)

        if count == 5:
            break  # stops while loop


if __name__ == "__main__":
    main()
