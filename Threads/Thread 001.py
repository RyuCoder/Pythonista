from pprint import pprint
import threading

print = pprint


class ModifiedThread(threading.Thread):
    def __init__(self, value, mod):
        threading.Thread.__init__(self)
        self.value = value
        self.mod = mod

    def run(self):
        print("Starting Thread " + str(self.value))
        func(self.value, self.mod)


def func(x, y):
    count = 0

    while True:

        if x % y == 0:

            if y == 3:
                print(str(x) + " from t1")
            else:
                print(str(x) + " from t2")

            x = x + y

        count += 1

        if count == 10:
            break


t1 = ModifiedThread(3, 3)
t2 = ModifiedThread(7, 7)

t1.start()
t2.start()


def main():
    threads = []
    threads.append(t1)
    threads.append(t2)

    for t in threads:
        t.join()


if __name__ == '__main__':
    main()
