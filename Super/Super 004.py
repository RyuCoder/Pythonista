
from pprint import pprint

print = pprint


class One():

    def main(self):
        print("Main of One")


class Base():

    def main(self):
        print("Main of Base")
        super(Base, self).main()


class Two(Base):

    def main(self):
        print("Main of Two")
        super(Two, self).main()


class Main(Two, One):
    def main(self):
        print("Main of Main")
        super(Main, self).main()


def main():
    main = Main()
    print(Main.mro())
    print("")
    main.main()


if __name__ == "__main__":
    main()
