
from pprint import pprint

print = pprint

class Base_One():

    def main(self):
        print("Main of Base_One")
        
        # Wont work as class object does not have a main function defined inside
        # super(Base_One, self).main() 


class One(Base_One):

    def main(self):
        print("Main of One")
        super(One, self).main()


class Base_Two():

    def main(self):
        print("Main of Base_Two")
        super(Base_Two, self).main()


class Two(Base_Two):

    def main(self):
        print("Main of Two")
        super(Two, self).main()


class Main(Two, One):
    def main(self):
        print("Main of Main")
        super(Main, self).main()


def main():
    main = Main()
    print(Main.mro())
    print("")
    main.main()


if __name__ == "__main__":
    main()
