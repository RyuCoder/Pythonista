
class One():

    def main(self):
        print("main of One")


class Main(One):
    def main(self):
        print("Main of Main")
        super(Main, self).main()


def main():
    main = Main()
    print(Main.mro())
    print()
    main.main()


if __name__ == "__main__":
    main()
