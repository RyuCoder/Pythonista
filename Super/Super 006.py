from pprint import pprint

print = pprint


class Base_One():

    def main(self):
        print("Main of Base_One")
        
        # Wont work as class object does not have a main function defined inside
        # super(Base_One, self).main() 


class One(Base_One):
    pass


class Two():
    pass


class Main(Two, One):
    def main(self):
        print("Main of Main")
        super(Main, self).main()


def main():
    main = Main()
    print(Main.mro())
    print("")
    main.main()


if __name__ == "__main__":
    main()
