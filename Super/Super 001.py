

class Main():
    def main(self):
        print("Main of Main")
        # super(Main, self).main() 
        # won't work as the class object in the mro list 
        # does not have a main function defined


def main():
    main = Main()
    print(Main.mro()) # mro() is on the class, not on instance of the class
    print()
    main.main()


if __name__ == "__main__":
    main()
